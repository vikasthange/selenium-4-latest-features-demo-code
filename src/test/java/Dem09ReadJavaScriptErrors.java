import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.HasDevTools;
import org.openqa.selenium.devtools.v95.log.Log;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;


public class Dem09ReadJavaScriptErrors {

    /**
     * @Author Vikas Thange
     */
    WebDriver driver;
    @BeforeMethod
    public void setup(){
        System.setProperty(
                "webdriver.chrome.driver",
                "/Users/vxt82/Downloads/chrome_driver_95/driver/mac64/chromedriver"
        );
    }
    @Test
    public void testJavaScriptErrors() throws IOException {
        driver = new ChromeDriver();
        try(DevTools devTools = ((HasDevTools)driver).getDevTools()){
            devTools.createSessionIfThereIsNotOne(); // Just in case if there is no session to DevTools
            List<JavascriptException> jsExceptionList = new ArrayList<>();
            Consumer<JavascriptException> addEntry = jsExceptionList::add;
            devTools.getDomains().events().addJavascriptExceptionListener(addEntry);

            driver.get("http://vikasthange.blogspot.com/2018/03/");

            WebElement btnSearch = driver.findElement(By.cssSelector("input.gsc-search-button"));
            ((JavascriptExecutor)driver).executeScript("arguments[0].setAttribute(arguments[1], arguments[2]);",btnSearch,"onClick","throw new error('My Error message')");

            btnSearch.click();

            for(JavascriptException exception: jsExceptionList){
                System.out.println("JS Exception Message: "+ exception.getMessage());
                System.out.println("JS Exception System info: "+exception.getSystemInformation());
                exception.printStackTrace();
            }
            Assert.assertEquals(jsExceptionList.size(),0,"JS Errors found");
        }
    }
    @AfterMethod
    public void tearDown(){
        driver.quit();
    }



}















