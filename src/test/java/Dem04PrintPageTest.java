import org.bouncycastle.util.encoders.Base64;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.print.PrintOptions;
import org.openqa.selenium.support.locators.RelativeLocator;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.List;


public class Dem04PrintPageTest {

    /**
     * @Author Vikas Thange
     */
    WebDriver driver;
    @BeforeSuite
    public void setup(){
        System.setProperty(
                "webdriver.chrome.driver",
                "/Users/vxt82/Downloads/chrome_driver_95/driver/mac64/chromedriver"
        );
    }
    @Test
    public void testPrintPageAsPDF() {
        driver = new ChromeDriver();
        String pageUrl = "https://www.browserstack.com/blog/"+
                "webinar-how-weather-com-achieves-quality-" +
                "at-speed-with-browserstack-and-jenkins/";
        driver.get(pageUrl);
        if(driver instanceof PrintsPage){
            PrintsPage printer = (PrintsPage) driver;
            //BUG: PrintToPDF is only supported in headless mode?
            PrintOptions printOptions = new PrintOptions();
            printOptions.setPageRanges("1-2");
            Pdf pdf = printer.print(printOptions);
            // Is getContent() is not suppose to decode and return only content?
            Assert.assertTrue(pdf.getContent().contains("Testing weather.com"));
        }
    }
    @AfterSuite
    public void tearDown(){
        driver.quit();
    }



}















