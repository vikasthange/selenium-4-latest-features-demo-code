import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.HasDevTools;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;


public class Dem10NetworkIntercepttingDemo {

    /**
     * @Author Vikas Thange
     */
    WebDriver driver;
    @BeforeMethod
    public void setup(){
        System.setProperty(
                "webdriver.chrome.driver",
                "/Users/vxt82/Downloads/chrome_driver_95/driver/mac64/chromedriver"
        );
    }
    @Test
    public void testJavaScriptErrors() throws IOException {
        driver = new ChromeDriver();
        try(DevTools devTools = ((HasDevTools)driver).getDevTools()){
            devTools.createSessionIfThereIsNotOne(); // Just in case if there is no session to DevTools

        }
    }
    @AfterMethod
    public void tearDown(){
        driver.quit();
    }



}















