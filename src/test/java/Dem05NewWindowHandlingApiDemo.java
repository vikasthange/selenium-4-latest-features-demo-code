import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.print.PrintOptions;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;


public class Dem05NewWindowHandlingApiDemo {

    /**
     * @Author Vikas Thange
     */
    WebDriver driver;
    @BeforeSuite
    public void setup(){
        System.setProperty(
                "webdriver.chrome.driver",
                "/Users/vxt82/Downloads/chrome_driver_95/driver/mac64/chromedriver"
        );
    }
    @Test
    public void testNewWindowHandlingAPI() {
        driver = new ChromeDriver();
        String pageUrl = "https://www.browserstack.com/blog/"+
                "webinar-how-weather-com-achieves-quality-" +
                "at-speed-with-browserstack-and-jenkins/";
        driver.get(pageUrl);
        driver.switchTo().newWindow(WindowType.TAB);
        driver.get("Https://google.com?q=vikas%20thange");
        Assert.assertEquals(
                driver.findElement(By.name("q")).getAttribute("value"),
                "vikas thange"
        );
    }
    @AfterSuite
    public void tearDown(){
        driver.quit();
    }



}















