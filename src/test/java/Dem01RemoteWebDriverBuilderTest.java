
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import java.net.MalformedURLException;
import java.net.URL;


public class Dem01RemoteWebDriverBuilderTest {

    /**
     * @Author Vikas Thange
     */
    WebDriver driver;
    @BeforeSuite
    public void setup(){
        System.setProperty(
                "webdriver.chrome.driver",
                "/Users/vxt82/Downloads/chrome_driver_95/driver/mac64/chromedriver"
        );
    }
    @Test
    public void testLaunchingBrowserOnLocal() throws MalformedURLException {
        // driver = new ChromeDriver(); // Works in both v3 & v4
        // Selenium 4 - New way to initialize Using RemoteWebDriver Builder
        driver =  RemoteWebDriver.builder()
                .oneOf(new ChromeOptions())
                .build();

    }
    @Test
    public void testLaunchBrowserOnRemoteMachine(){
        // To Start browser on remote machine
        driver =  RemoteWebDriver.builder()
                .oneOf(new ChromeOptions())
                .address("http://remote_ip:port") // Your remote host & port no / cloud url
                .build();
    }


}
