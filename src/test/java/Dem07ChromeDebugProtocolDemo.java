import org.openqa.selenium.By;
import org.openqa.selenium.HasAuthentication;
import org.openqa.selenium.UsernameAndPassword;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.HasDevTools;
import org.openqa.selenium.devtools.v95.dom.model.Rect;
import org.openqa.selenium.devtools.v95.page.Page;
import org.openqa.selenium.devtools.v95.page.model.Viewport;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Files;

import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Optional;


public class Dem07ChromeDebugProtocolDemo {

    /**
     * @Author Vikas Thange
     */
    WebDriver driver;
    @BeforeMethod
    public void setup(){
        System.setProperty(
                "webdriver.chrome.driver",
                "/Users/vxt82/Downloads/chrome_driver_95/driver/mac64/chromedriver"
        );
    }
    @Test
    public void exampleChromeDebuggingProtocol() throws IOException {
        driver = new ChromeDriver();
        try(DevTools devTools = ((HasDevTools)driver).getDevTools()){
            devTools.createSessionIfThereIsNotOne(); // Just in case if there is no session to DevTools
            devTools.addListener(
                    Page.loadEventFired(),
                    event -> System.out.println("Load event at "+ event)
            );
            devTools.send(Page.enable());
            Page.GetLayoutMetricsResponse metrics = devTools.send(Page.getLayoutMetrics());
            Rect size = metrics.getCssContentSize();
            driver.get("http://vikasthange.blogspot.com/");
            String result = devTools.send(Page.captureScreenshot(
                    Optional.empty(),
                    Optional.empty(),
                    Optional.of(new Viewport(0,0,size.getWidth(),size.getHeight(),1)),
                    Optional.empty(),
                    Optional.of(true)
                    )
            );
            Files.write(Paths.get("snapshot.png"), Base64.getDecoder().decode(result));
        }
    }
    @AfterMethod
    public void tearDown(){
        driver.quit();
    }



}















