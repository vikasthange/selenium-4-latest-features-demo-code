import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.net.MalformedURLException;


public class Dem02DesiredCapabilitiesTest {

    /**
     * @Author Vikas Thange
     */
    WebDriver driver;
    @BeforeSuite
    public void setup(){
        System.setProperty(
                "webdriver.chrome.driver",
                "/Users/vxt82/Downloads/chrome_driver_95/driver/mac64/chromedriver"
        );
    }
    @Test
    public void testLaunchingBrowserOnLocal() throws MalformedURLException {
        // Valid in both version
        driver = new ChromeDriver();

        // works only in v3 and no longer works in v4
//        driver = new ChromeDriver(DesiredCapabilities.chrome());

        // works only in both but deprecated in v4
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setBrowserName("chrome");
        driver = new ChromeDriver(caps);

        // Selenium 4 - recommended
        driver = new ChromeDriver(new ChromeOptions());
    }



}
