import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;


public class Dem06BasicAndDigestAuth {

    /**
     * @Author Vikas Thange
     */
    WebDriver driver;
    @BeforeMethod
    public void setup(){
        System.setProperty(
                "webdriver.chrome.driver",
                "/Users/vxt82/Downloads/chrome_driver_95/driver/mac64/chromedriver"
        );
    }
    @Test
    public void testBasicAuth() {
        driver = new ChromeDriver();
        HasAuthentication auth = (HasAuthentication) driver;
        auth.register( () ->
                new UsernameAndPassword("admin","admin")
        );
        driver.get("https://the-internet.herokuapp.com");
        driver.findElement(By.linkText("Basic Auth")).click();
        String actual = driver.findElement(By.cssSelector(".example p")).getText();
        Assert.assertTrue(actual.contains("Congratulations!"));
    }
    @Test
    public void testDigestAuth() {
        driver = new ChromeDriver();
        HasAuthentication auth = (HasAuthentication) driver;
        auth.register( () ->
                new UsernameAndPassword("admin","admin")
        );
        driver.get("https://the-internet.herokuapp.com");
        driver.findElement(By.linkText("Digest Authentication")).click();
        String actual = driver.findElement(By.cssSelector(".example p")).getText();
        Assert.assertTrue(actual.contains("Congratulations!"));
    }
    @AfterMethod
    public void tearDown(){
        driver.quit();
    }



}















