import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.locators.RelativeLocator;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.List;


public class Dem03RelativeLocatorsDemoTest {

    /**
     * @Author Vikas Thange
     */
    WebDriver driver;
    @BeforeSuite
    public void setup(){
        System.setProperty(
                "webdriver.chrome.driver",
                "/Users/vxt82/Downloads/chrome_driver_95/driver/mac64/chromedriver"
        );
    }
    @Test
    public void testRelativeLocatorExample() {
        driver = new ChromeDriver();
        driver.get("https://www.browserstack.com/blog/"+
                "webinar-how-weather-com-achieves-quality-" +
                "at-speed-with-browserstack-and-jenkins/");
        List<WebElement> allNextArticles = driver.findElements(
                By.cssSelector(".read-next-feed article")
        );
        WebElement middle = allNextArticles.get(1);
        WebElement rightArticle = driver.findElement(
                RelativeLocator.with(By.cssSelector("article")).toRightOf(middle)
        );
        Assert.assertTrue(
                rightArticle.getText().contains("Microsoft Edge 79"),
                "Incorrect right article"
        );
    }
    @AfterSuite
    public void tearDown(){
        driver.quit();
    }



}















