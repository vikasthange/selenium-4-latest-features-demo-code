import org.apache.hc.core5.http.HttpStatus;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.HasDevTools;
import org.openqa.selenium.devtools.NetworkInterceptor;
import org.openqa.selenium.devtools.v95.dom.model.Rect;
import org.openqa.selenium.devtools.v95.log.Log;
import org.openqa.selenium.devtools.v95.page.Page;
import org.openqa.selenium.devtools.v95.page.model.Viewport;
import org.openqa.selenium.remote.http.HttpResponse;
import org.openqa.selenium.remote.http.Route;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Optional;


public class Dem08ReadConsoleLogMessages {

    /**
     * @Author Vikas Thange
     */
    WebDriver driver;
    @BeforeMethod
    public void setup(){
        System.setProperty(
                "webdriver.chrome.driver",
                "/Users/vxt82/Downloads/chrome_driver_95/driver/mac64/chromedriver"
        );
    }
    @Test
    public void testBrowserConsoleLogMessages() throws IOException, InterruptedException {
        driver = new ChromeDriver();
        try(DevTools devTools = ((HasDevTools)driver).getDevTools()){
            devTools.createSessionIfThereIsNotOne(); // Just in case if there is no session to DevTools
            HttpResponse response = new HttpResponse()
                    .setStatus(HttpStatus.SC_MOVED_TEMPORARILY)
                    .setHeader("Location","https://avatars.githubusercontent.com/u/13792517");
            Route route = Route.combine(
                    Route.get("https://shop.demoqa.com/wp-content/uploads/2019/04/black-cross-back-maxi-dress-650x650.jpg").to(()->req -> response),
                    Route.get("https://shop.demoqa.com/wp-content/uploads/2019/04/black-lux-graphic-t-shirt-650x650.jpg").to(()->req -> response),
                    Route.get("https://shop.demoqa.com/wp-content/uploads/2019/04/black-pointed-toe-barely-there-patent-heels-650x650.jpg").to(()->req -> response)

                    );
            NetworkInterceptor networkInterceptor = new NetworkInterceptor(driver,route);

            driver.get("https://shop.demoqa.com/shop/");
            Thread.sleep(15000);
        }
    }
    @AfterMethod
    public void tearDown(){
        driver.quit();
    }



}















